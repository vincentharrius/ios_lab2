//
//  SkillsViewController.swift
//  vincent_harrius_cv
//
//  Created by Vincent Harruis on 2018-12-07.
//  Copyright © 2018 Vincent Harrius. All rights reserved.
//

import UIKit
import CoreMotion
import CoreGraphics

class SkillsViewController: UIViewController {
    
    
    @IBOutlet weak var circleView: UIView!
    
    let motions: CMMotionManager = CMMotionManager()
    
    var circleCentre : CGPoint!
    var newCircleCentre : CGPoint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        circleView.layer.cornerRadius = 64
        
        circleCentre = circleView.center
        newCircleCentre = circleView.center
        
        self.title = "Skills"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        motions.accelerometerUpdateInterval = 1 / 100
        if (motions.isDeviceMotionAvailable) {
            motions.startAccelerometerUpdates(to: OperationQueue.current!) { (data, error) in
                if let movementData = data {
                    
                    self.newCircleCentre.x = (CGFloat(movementData.acceleration.x) * 10)
                    self.newCircleCentre.y = (CGFloat(movementData.acceleration.y) * -10)
                    
                    if abs(self.newCircleCentre.x) + abs(self.newCircleCentre.y) < 1.0 {
                        self.newCircleCentre = .zero
                    }
                    
                    self.circleCentre = CGPoint(x: self.circleCentre.x + self.newCircleCentre.x, y: self.circleCentre.y + self.newCircleCentre.y)
                    
                    self.circleCentre.x = max(self.circleView.frame.size.width / 2, min(self.circleCentre.x, self.view.bounds.width - self.circleView.frame.size.width / 2))
                    
                    self.circleCentre.y = max(self.circleView.frame.size.height / 2, min(self.circleCentre.y, self.view.bounds.height - self.circleView.frame.size.height / 2))
                    
                    
                    self.circleView.center = self.circleCentre
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
