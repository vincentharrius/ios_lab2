//
//  ExperienceDetailViewController.swift
//  vincent_harrius_cv
//
//  Created by Vincent Harruis on 2018-12-07.
//  Copyright © 2018 Vincent Harrius. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var experience: Experience?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = experience?.name
        image.image = experience?.relevantImage
        titleLabel.text = experience?.name
        dateLabel.text = experience?.get_dates()
        descriptionLabel.text = experience?.desc
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
