//
//  ExperienceTableViewController.swift
//  vincent_harrius_cv
//
//  Created by Vincent Harruis on 2018-12-05.
//  Copyright © 2018 Vincent Harrius. All rights reserved.
//

import UIKit

class ExperienceTableViewController: UITableViewController {

    var experiences: [Experience] = []
    
    var workCounter: Int = 0
    var educationCounter: Int = 0
    
    var selectedExperience: Experience?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self

        experiences.append(Experience(name: "Carefox AB", startDate: "2018-04-04", title: "Junior Utvecklare", imageName: "feature", desc: sampleText))
        experiences.append(Experience(name: "Hiko.se", startDate: "2013-10-01", endDate: "2016-08-21", title: "Lagerarbetare", imageName: "box", desc: sampleText))
        experiences.append(Experience(name: "City Hotell Familjen Ericsson", startDate: "2016-04-04", title: "Receptionist vid behov", imageName: "hotel", desc: sampleText))
        experiences.append(Experience(name: "Junegårdens äldreboende", startDate: "2013-06", endDate: "2013-08", title: "Sommarjobbare", imageName: "first-aid-kit", desc: sampleText))
        
        experiences.append(Experience(name: "John Bauer-Gymnasiet", startDate: "2010-08-16", endDate: "2013-06-12", title: "Gymnasiet - Teknik IT", imageName: "pencil", type: "education", desc: sampleText))
        experiences.append(Experience(name: "Jönköping University", startDate: "2016-08-22", title: "Högskoleingenjör - Mjukvarutveckling och mobile plattformar", imageName: "mortarboard", type: "education", desc: sampleText))
        
        for experience in experiences {
            if experience.type == "work" { workCounter += 1 }
            else if experience.type == "education" { educationCounter += 1 }
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return workCounter
        }
        else if(section == 1) {
            return educationCounter
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0) {
            return "Work Experience"
        }
        else if(section == 1) {
            return "Education"
        }
        return "what is dis section?"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var rowOffset: Int = 0
        
        if(indexPath.section == 1) {
            rowOffset = workCounter
        }
        
        let experience = experiences[indexPath.row + rowOffset]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceTableViewCell") as! ExperienceTableViewCell

        cell.setupCell(experience: experience)
        
        return cell
    }
 
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var rowOffset: Int = 0
        if (indexPath.section == 1) {
            rowOffset = workCounter
        }
        selectedExperience = experiences[indexPath.row + rowOffset]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let selectedCell: ExperienceTableViewCell = sender as! ExperienceTableViewCell
        
        if segue.identifier == "ShowDetail"
        {
            if let destinationVC = segue.destination as? ExperienceDetailViewController {
                destinationVC.experience = selectedCell.selectedExperience
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

        let sampleText = "The potato is a starchy, tuberous crop from the perennial nightshade Solanum tuberosum. In many contexts, potato refers to the edible tuber, but it can also refer to the plant itself. Common or slang terms include tater and spud. Potatoes were introduced to Europe in the second half of the 16th century by the Spanish. Today they are a staple food in many parts of the world and an integral part of much of the world's food supply. As of 2014, potatoes were the world's fourth-largest food crop after maize (corn), wheat, and rice."
}
