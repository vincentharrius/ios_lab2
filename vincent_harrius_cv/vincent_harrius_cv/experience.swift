//
//  Work.swift
//  vincent_harrius_cv
//
//  Created by Vincent Harruis on 2018-12-05.
//  Copyright © 2018 Vincent Harrius. All rights reserved.
//

import Foundation
import UIKit

class Experience: NSObject {
    
    var name: String
    var startDate: String
    var endDate: String
    var title: String
    var type: String
    var desc: String
    
    var relevantImage: UIImage
    
    init(name: String, startDate: String, endDate: String = "Current", title: String, imageName: String, type: String = "work", desc: String = "no description") {
        self.name = name
        self.startDate = startDate
        self.endDate = endDate
        self.title = title
        self.type = type
        self.desc = desc
        
        self.relevantImage = UIImage(named: imageName)!
    }
    
    func get_dates() -> String {
        return startDate + " - " + endDate
    }
}
