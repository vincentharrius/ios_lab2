//
//  ViewController.swift
//  vincent_harrius_cv
//
//  Created by Vincent Harruis on 2018-12-05.
//  Copyright © 2018 Vincent Harrius. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImage.image = UIImage(named: "vincent-final")
    }


}

