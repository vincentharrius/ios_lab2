//
//  ExperienceTableViewCell.swift
//  vincent_harrius_cv
//
//  Created by Vincent Harruis on 2018-12-05.
//  Copyright © 2018 Vincent Harrius. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {


    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var yearsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
    var selectedExperience: Experience? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
    }

    func setupCell(experience: Experience) {
        self.selectedExperience = experience
        
        nameLabel.text = experience.name
        yearsLabel.text = experience.get_dates()
        titleLabel.text = experience.title
        
        cellImage.image = experience.relevantImage
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
